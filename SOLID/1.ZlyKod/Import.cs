﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace SOLID._1.ZlyKod
{
    public class Import
    {
        public static void Run()
        {

            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Imporcik.txt");
            if (stream == null)
            {
                Console.WriteLine("Nie ma pliku!");
            }


            var linieImportu = new List<string>();
            using (var reader = new StreamReader(stream))
            {
                string linia;
                while ((linia = reader.ReadLine()) != null)
                {
                    linieImportu.Add(linia);
                }
            }

            var dane = new List<string[]>();

            var count = 1;
            foreach (var line in linieImportu)
            {
                var wiersz = line.Split(new char[] { '|' });

                if (wiersz.Length != 4)
                {
                    Console.WriteLine("Linia {0} jest błędna. Za mało pól.", count);
                    count++;
                    continue;
                }

                if (wiersz[0].Length > 20)
                {
                    Console.WriteLine("Identyfikator w lini {0} jest zbyt długi.", count);
                    count++;
                    continue;
                }

                if (wiersz[1].Length > 100)
                {
                    Console.WriteLine("Nazwa w lini {0} jest zbyt długa.", count);
                    count++;
                    continue;
                }

                if (wiersz[2].Length > 500)
                {
                    Console.WriteLine("Opis w lini {0} jest zbyt długi.", count);
                    count++;
                    continue;
                }

                try
                {
                    decimal cena = decimal.Parse(wiersz[3]);
                }
                catch 
                {
                    Console.WriteLine("Cena w lini {0} jest niepoprawna.", count);
                    count++;
                    continue;
                }

                if (wiersz[4].Length != 3)
                {
                    Console.WriteLine("Waluta w lini {0} jest zbyt długa.", count);
                    count++;
                    continue;
                }
                dane.Add(wiersz);
                count++;
            }

            using (var connection = new System.Data.SqlClient.SqlConnection("Data Source=(local);Initial Catalog=Testowa;Integrated Security=True;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    foreach (var wiersz in dane)
                    {
                        var command = connection.CreateCommand();
                        command.Transaction = transaction;
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "dbo.dane";
                        command.Parameters.AddWithValue("@Identyfikator", wiersz[0]);
                        command.Parameters.AddWithValue("@Nazwa", wiersz[1]);
                        command.Parameters.AddWithValue("@Opis", wiersz[2]);
                        command.Parameters.AddWithValue("@Cena", wiersz[3]);
                        command.Parameters.AddWithValue("@Waluta", wiersz[4]);

                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
                connection.Close();
            }

            Console.WriteLine("INFO: {0} trades processed", dane.Count);
        }
    }
}

