﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace SOLID._2.Lepiej
{
    public class Import
    {
        public void Run(string plikSciezka)
        {

            var linieImportu = CzytajZPliku(plikSciezka);
            var dane = new List<Dane>();
            for (int i = 0; i < linieImportu.Count; i++)
            {
                var line = linieImportu[i];
                var wiersz = line.Split('|');
                if (WalidacjaWiersza(wiersz, i + 1))
                {
                    dane.Add(Parsuj(wiersz));
                }
            }
            ZapiszDane(dane);
            Console.WriteLine("Koniec: {0} wczytanych wierszy na {1} wszystkich", linieImportu, dane.Count);
        }

        private static List<string> CzytajZPliku(string plikSciezka)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(plikSciezka);
            if (stream == null)
            {
                Console.WriteLine("Nie ma pliku!");
            }

            var linieImportu = new List<string>();
            using (var reader = new StreamReader(stream))
            {
                string linia;
                while ((linia = reader.ReadLine()) != null)
                {
                    linieImportu.Add(linia);
                }
            }
            return linieImportu;
        }

        private void ZapiszDane(List<Dane> lista)
        {
            using (
                var connection =
                    new System.Data.SqlClient.SqlConnection(
                        "Data Source=(local);Initial Catalog=Testowa;Integrated Security=True;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    foreach (var dane in lista)
                    {
                        var command = connection.CreateCommand();
                        command.Transaction = transaction;
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "dbo.dane";
                        command.Parameters.AddWithValue("@Identyfikator", dane.Identyfikator);
                        command.Parameters.AddWithValue("@Nazwa", dane.Nazwa);
                        command.Parameters.AddWithValue("@Opis", dane.Opis);
                        command.Parameters.AddWithValue("@Cena", dane.Cena);
                        command.Parameters.AddWithValue("@Waluta", dane.Waluta);
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
                connection.Close();
            }
        }

        private bool WalidacjaWiersza(string[] wiersz, int count)
        {
            if (wiersz.Length != 4)
            {
                Console.WriteLine("Linia {0} jest błędna. Za mało pól.", count);
                return false;
            }

            if (wiersz[0].Length > 20)
            {
                Console.WriteLine("Identyfikator w lini {0} jest zbyt długi.", count);
                return false;
            }

            if (wiersz[1].Length > 100)
            {
                Console.WriteLine("Nazwa w lini {0} jest zbyt długa.", count);
                return false;
            }

            if (wiersz[2].Length > 500)
            {
                Console.WriteLine("Opis w lini {0} jest zbyt długi.", count);
                return false;
            }

            decimal cena;
            if (decimal.TryParse(wiersz[3], out cena))
            {
                Console.WriteLine("Cena w lini {0} jest niepoprawna.", count);
                return false;
            }

            if (wiersz[4].Length != 3)
            {
                Console.WriteLine("Waluta w lini {0} jest zbyt długa.", count);
                return false;
            }

            return true;
        }

        private Dane Parsuj(string[] wiersz)
        {
            Dane dane = new Dane()
            {
                Identyfikator = wiersz[0],
                Nazwa = wiersz[1],
                Opis = wiersz[2],
                Cena = decimal.Parse(wiersz[3]),
                Waluta = wiersz[4]
            };
            return dane;
        }


    }
}
