﻿using System.Collections.Generic;

namespace SOLID._3.Dobrze.Contracts
{
    public interface IDaneParser
    {
        IEnumerable<Dane> ParsujDane(string[] linieImportu);
    }
}
