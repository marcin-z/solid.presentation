﻿namespace SOLID._3.Dobrze.Contracts
{
    public interface IDaneWalidator
    {
        bool Sprawdz(string[] wiersz, int count);
    }
}
