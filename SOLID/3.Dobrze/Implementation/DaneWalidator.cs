﻿using System;
using SOLID._3.Dobrze.Contracts;

namespace SOLID._3.Dobrze.Implementation
{
    public class DaneWalidator : IDaneWalidator
    {
        private readonly ILog _log;

        public DaneWalidator(ILog log)
        {
            _log = log;
        }

        public bool Sprawdz(string[] wiersz, int count)
        {
            if (wiersz.Length != 4)
            {
                _log.LogInfo("Linia {0} jest błędna. Za mało pól.", count);
                return false;
            }

            if (wiersz[0].Length > 20)
            {
                _log.LogInfo("Identyfikator w lini {0} jest zbyt długi.", count);
                return false;
            }

            if (wiersz[1].Length > 100)
            {
                _log.LogInfo("Nazwa w lini {0} jest zbyt długa.", count);
                return false;
            }

            if (wiersz[2].Length > 500)
            {
                _log.LogInfo("Opis w lini {0} jest zbyt długi.", count);
                return false;
            }

            decimal cena;
            if (decimal.TryParse(wiersz[3], out cena))
            {
                Console.WriteLine("Cena w lini {0} jest niepoprawna.", count);
                return false;
            }

            if (wiersz[4].Length != 3)
            {
                Console.WriteLine("Waluta w lini {0} jest zbyt długa.", count);
                return false;
            }

            return true;
        }
    }
}
