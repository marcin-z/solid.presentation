﻿using SOLID._3.Dobrze;
using SOLID._3.Dobrze.Contracts;
using SOLID._3.Dobrze.Implementation;

namespace SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            ILog log = new ConsoleLog();
            IDaneZrodlo daneZrodlo = new DaneZrodlo(log, "import.txt");
            IDaneWalidator daneWalidator = new DaneWalidator(log);
            IDaneParser daneParser = new DaneParser(daneWalidator,log);
            IDaneMagazyn daneMagazyn = new SqlServerMagazyn(log);

            var daneImport = new DaneImport(daneZrodlo,daneParser,daneMagazyn);
            daneImport.Importuj();
        }
    }
}
